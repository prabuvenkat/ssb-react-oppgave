## Requirements/Technologies

- [NodeJS](https://nodejs.org/en/) v10.16.+ (NPM: v6.9+)
- [React](https://reactjs.org) v16.9.+ (uses [Hooks](https://reactjs.org/docs/hooks-intro.html))
- [Highcharts](https://highcharts.com) ([react-version](https://github.com/highcharts/highcharts-react))

----

## Json Stat Playground ##

A tiny PoC to easily render/view a JSON-Dataset and more importantly also generate an easily sharable PNG 
of rendered chart.

### Requirements
- [Highcharts Export Server](https://github.com/highcharts/node-export-server) running on localhost:7801
  
----

## Setup

```
$ git clone https://gitlab.com/prabuvenkat/ssb-react-oppgave.git
$ cd ssb-react-oppgave
$ npm i
$ npm run start
```

Point browser to http://localhost:8080/index.html

## Tracking

### **MileStones**
  - ~~v1.2~~ : ~~[JSON Stat PoC](%3)~~
  - **v1.1** : [Allow upload of new wordlist](%2)
  - ~~**v1.0**~~ : ~~[Basic functionality](%1)~~

### **Known Issues**

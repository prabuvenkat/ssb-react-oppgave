import React, { useState, useEffect } from 'react';
import Main from './Main.jsx';
import FfF from './FfF.jsx';
import appStyles from './main.css';

const ANAGRAMS = <Main />;
const JSONSTAT = <FfF />;

const APP_VIEW = {
    ANAGRAMS: 'Anagrams',
    JSONSTAT: 'Json Stat Playground',
};

const getView = (currView) => {
    switch(currView) {
        case APP_VIEW.ANAGRAMS: return ANAGRAMS;
        default: return JSONSTAT;
    }
};

const getViewName = (currView) => {
    switch(currView) {
        case APP_VIEW.ANAGRAMS: return 'Json Playground';
        default: return 'Anagrams';
    }
};

const App = () => {
    const [currView, setCurrView] = useState(APP_VIEW.ANAGRAMS);
    const [view, setView] = useState();

    useEffect(() => {
        if (currView) {
            setView(getView(currView));
        }
    }, [currView]);

    const switchLink = () => {
        switch(currView) {
            case APP_VIEW.ANAGRAMS: { setCurrView(APP_VIEW.JSONSTAT); break; }
            default: { setCurrView(APP_VIEW.ANAGRAMS); }
        }
    };

    return (
        <div>
            <div className={appStyles.headerBlock}>
                <h1>Fakta før følelser !</h1>
                <div>
                    <a className={appStyles.simpleLink} onClick={switchLink}>{getViewName(currView)}</a>
                </div>
            </div>
            {getView(currView)}
        </div>
    );
};

export default App;
import React from 'react';
import JsonStatPlayground from './scenes/jsonstat/JsonStatPlayground.jsx';
import Dataset from './model/Dataset';

import EMP_MOCK from './data/08931';
const DATASET = new Dataset(EMP_MOCK);

const FfF = () => <JsonStatPlayground dataset={DATASET} />;

export default FfF;
import React, { useState, useEffect } from 'react';
import AnagramsTable from './scenes/AnagramsTable.jsx';
import AnagramsFrequencyPlot from './scenes/AnagramsFrequencyPlot.jsx';
import { computeAnagrams } from './utils/stringUtils';

import appStyles from './main.css';

// load content statically for now
import FILE_CONTENT from './data/eventyr.txt';
// TBD (time permitting)
const ALLOW_EDIT_WORDLIST = false;

const DEFAULT_WORD_LIST = FILE_CONTENT.split('\r\n');

const Main = () => {
    const [wordList, setWordList] = useState(DEFAULT_WORD_LIST);
    const [anagrams, setAnagrams] = useState([]);

    useEffect(() => {
        if (Array.isArray(wordList)) {
            setAnagrams(computeAnagrams(wordList));
        }
    }, [wordList]);

    // TBD (time permitting)
    const updateWordList = ALLOW_EDIT_WORDLIST ? (
        <div>
            <button className={appStyles.refresh}>
                <span>Endre ordliste</span>
            </button>
        </div>
    ) : null;

    return (
        <div className={appStyles.mainScene}>
            <div className={appStyles.anagramResults}>
                <AnagramsTable anagrams={anagrams} totalWordCount={wordList.length}/>
            </div>
            <div className={appStyles.anagramViz}>
                <AnagramsFrequencyPlot anagrams={anagrams} />
            </div>
        </div>
    );
};

export default Main;
import { makeVariableOptions } from '../utils/jsonstatUtils';
import Dataset from '../model/Dataset';

import EMP_MOCK from '../data/08931';

const dataset = new Dataset(EMP_MOCK);

describe('jsonstat dataset', () => {
    it('should load properly', () => {
        const dims = dataset.dimensions;
       expect(dataset).toBeTruthy();
       expect(dataset.dimensions).toBeTruthy();
       expect(Array.isArray(dataset.dimensions)).toBe(true);
       expect(dataset.dimensions.length > 0).toBe(true);

       expect(dataset.roles.time).toBeTruthy();
       expect(dataset.roles.metric).toBeTruthy();
    });

    it('should be possible to make dropdown for dimensions', () => {
        const options = makeVariableOptions(dataset.dimensions);
        expect(options).toBeTruthy();
    });
});
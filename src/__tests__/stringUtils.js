import { sortChars, computeAnagrams } from '../utils/stringUtils';

describe('sortChars', () => {
    it('should return word with characters sorted', () => {
        const word = 'program';
        expect(sortChars(word)).toBe('agmoprr');
        expect(sortChars(word, { reverse: true })).toBe('rrpomga');
    });
});

describe('anagram finder', () => {
    it('should return all angarams in input set', () => {
        const wordList = [
            'hello',
            'how',
            'ehlol',
            'who',
            'me',
            'they',
            'hey',
            'holle',
            'work',
        ];

        const anagrams = computeAnagrams(wordList);
        expect(anagrams.length).toBe(2);
    });


    it('should find anagrams independent of case', () => {
        const wordList = [
            'hello',
            'how',
            'ehlol',
            'WHO',
            'me',
            'they',
            'hey',
            'holle',
            'work',
        ];

        const anagrams = computeAnagrams(wordList);
        expect(anagrams.length).toBe(2);
    });

    it('should handle null, empty strings just fine', () => {
        const wordList = [
            'hello',
            'how',
            'ehlol',
            'WHO',
            'they',
            '',
            null,
            'hey',
            'holle',
            'work',
        ];

        const anagrams = computeAnagrams(wordList);
        expect(anagrams.length).toBe(2);
    });
});
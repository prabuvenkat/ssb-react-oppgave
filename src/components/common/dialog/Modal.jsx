import React from 'react';

import styles from './modal.css';

const Modal = ({
        show,
        onConfirm, confirmText = 'OK',
        showCancel = 'true', onCancel, cancelText = 'Cancel',
        footerComponent,
        children,
        style = {}, styleClass = '',
    }) => {
    if (!show)
        return false;

    const getFooter = () => {
        if (footerComponent) {
            return (
                <div className={styles.modalFooter}>
                    {footerComponent}
                </div>
            );
        }

        const cancelBtn = (showCancel) ? (
            <button className="button button--secondary" onClick={onCancel}>
                {cancelText}
            </button>
        ) : null;

        return (
            <div className={styles.modalFooter}>
                <button className="button button--primary " onClick={onConfirm}>
                    {confirmText}
                </button>
                {cancelBtn}
            </div>
        );
    };

    return (
        <div className={styles.modalOverlay}>
            <div className={`${styles.modalDialog} ${styleClass}`} style={style}>
                <div className={styles.modalContent}>
                    {children}
                </div>
                {getFooter()}
            </div>
        </div>
    );
};

export default Modal;

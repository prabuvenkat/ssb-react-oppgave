import React, { useState } from 'react';

import styles from './dropdown.css';

const DropDown = (props) => {
    const [show, setShow] = useState(false);
    const toggleShow = () => setShow(!show);

    const { id, options, onChange, selection, disabled, renderOption, styleClass, prefix = null } = props;
    return (
        <div className={styles.dropdown}>
            <button
                id={`${id}-btn`}
                className={styles.dropdownBtn}
                onClick={toggleShow}
                disabled={disabled}
            >
                {prefix && <span className={styles.prefix}>{prefix}</span>} {options[selection]}
                <i className="material-icons">arrow_drop_down</i>
            </button>
            {show &&
                <div className={styles.dropdownItems}>
                    {Object.keys(options)
                        .filter(k => k !== '--')
                        .map(k => <a onClick={() => {
                        onChange({ target: { value: k }});
                        toggleShow();
                    }}>{(typeof renderOption === 'function') ? renderOption(k) : options[k]}</a>)}
                </div>
            }
        </div>
    );
};

export default DropDown;
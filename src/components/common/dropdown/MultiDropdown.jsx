import React, { useState, useEffect } from 'react';
import styles from './dropdown.css';

const MultiDropdown = (props) => {
    const {
        id,
        label,
        options,
        onChange,
        renderOption,
        selections = [],
        disabled = false,
        styleClass = ''
    } = props;

    const [show, setShow] = useState(false);
    const toggleShow = () => setShow(!show);

    const [selectionOptions, setSelectionOptions] = useState({});

    const toggleOption = (o) => {
        setSelectionOptions({
            ...selectionOptions,
            [o]: !selectionOptions[o],
        });
    };

    const handleFinish = () => {
        onChange(id, Object.keys(selectionOptions).filter(o => selectionOptions[o]));
        toggleShow();
    };

    const makeCheckbox = (opt) => {
        console.log('makeChkbox', selectionOptions, opt);
        return (
            <div className={styleClass}>
                <input
                    type="checkbox"
                    checked={selectionOptions[opt]}
                    onChange={() => { toggleOption(opt); }}
                    value={1}
                />
                <a onClick={() => { toggleOption(opt); }}>
                    {(typeof renderOption === 'function') ? renderOption(opt) : options[opt]}
                </a>
            </div>
        );
    };

    return (
        <div className={styles.dropdown}>
            <button
                id={`${id}-btn`}
                className={styles.dropdownBtn}
                onClick={toggleShow}
                disabled={disabled}
            >
                {`Select ${label || id}`}
                <i className="material-icons">arrow_drop_down</i>
            </button>
            {show &&
                <div className={styles.dropdownItems}>
                    {Object.keys(options).map(k => makeCheckbox(k))}
                    <div className={styles.actionBar}>
                        <button className={styles.actionButton} onClick={handleFinish}>Done</button>
                    </div>
                </div>
            }
        </div>
    );
};

export default MultiDropdown;
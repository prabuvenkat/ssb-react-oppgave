import React, {Component} from 'react';
import PropTypes from 'prop-types';
import uuidv4 from 'uuid/v4';
import TableColSpec from './shapes/TableColSpec';
import styles from './table.css';
import { simpleLink } from '../../../main.css';

export const TABLE_SORT_ORDER_NONE = 'NONE';
export const TABLE_SORT_ORDER_ASC = 'ASCENDING';
export const TABLE_SORT_ORDER_DESC = 'DESCENDING';

export const DEFAULT_COLUMN_COMPARATOR = (colInfo) => (a, b) => {
    const fieldA = colInfo.extract(a);
    const fieldB = colInfo.extract(b);

    if (fieldA < fieldB) {
        return 1;
    } else if (fieldA > fieldB) {
        return -1;
    }

    return 0;
};

const sort = (data, colSpec, colId, order) => {
    const colInfo = colSpec.find((cs) => { return cs.id === colId; });
    const factor = (order === TABLE_SORT_ORDER_DESC) ? 1 : -1;
    const compareByField = colInfo.sortFn ? colInfo.sortFn : DEFAULT_COLUMN_COMPARATOR(colInfo);

    return data.sort((a, b) => (factor * compareByField(a, b)));
};

class Table extends Component {
    constructor(props, defaultProps) {
        super(props, defaultProps);
        this.state = {
            pageSize: props.pageSize || defaultProps.pageSize || 3,
            currPage: 1,
            expandedRow: null,
            currSort: props.initSort || {
                column: null,
                order: TABLE_SORT_ORDER_NONE,
            },
        };

        this.getSortLink = this.getSortLink.bind(this);
        this.toggleSortOrder = this.toggleSortOrder.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
    }

    prevPage() {
        this.setState({
            ...this.state,
            currPage: this.state.currPage - 1,
        });
    }

    nextPage() {
        this.setState({ ...this.state, currPage: this.state.currPage + 1 })
    }

    toggleSortOrder(colId) {
        const {
            order
        } = this.state.currSort;

        const newOrder = () => {
            switch (order) {
                case TABLE_SORT_ORDER_ASC: return TABLE_SORT_ORDER_DESC;
                default: return TABLE_SORT_ORDER_ASC;
            }
        };

        this.setState({
            ...this.state,
            currPage: 1,
            currSort: {
                ...this.state.currSort,
                order: newOrder(),
                column: colId,
            },
            expandedRow: null,
        });
    }

    getSortLink(colId) {
        const {
            currSort,
        } = this.state;

        const icon = () => {
            if (colId !== currSort.column) {
                return 'swap_vert';
            }

            switch (currSort.order) {
                case TABLE_SORT_ORDER_DESC: return 'arrow_downward';
                case TABLE_SORT_ORDER_ASC: return 'arrow_upward';
                default: return 'swap_vert';
            }
        }

        return (
            <a
                onClick={() => { this.toggleSortOrder(colId); }}
                className={(colId === currSort.column) ? styles[`sort-${currSort.order}`] : styles[`sort-NONE`]}
            >
                <i className="material-icons">{icon()}</i>
            </a>
        );
    }

    render() {
        const {
            colSpec,
            data,
            noDataText,
            showRowNumbers,
            showPagination,
            totalRecordsText,
            paginationPrefix,
            rowExpansion,
            onRowClick,
            styleClass,
            initSort,
        } = this.props;

        if (data && Array.isArray(data) && data.length > 0) {

            const getRows = () => {
                const rowNbrHdr
                    = (showRowNumbers)
                    ? <th key={uuidv4()} className={styles.rowNumber}>#</th>
                    : null;

                const colHdrs = colSpec.map((colInfo) => {
                    const header
                        = (colInfo.sortable)
                        ? (
                            <div className={styles.sortableColumn}>
                                {colInfo.title}{this.getSortLink(colInfo.id)}
                            </div>
                        ) : colInfo.title;

                    const headerClassNames = `${colInfo.sortable ? styles.sortable: ''} ${colInfo.headerStyleClass || ''}`;

                    return (
                        <th key={uuidv4()} className={headerClassNames}>
                            {header}
                        </th>
                    );
                });

                const headers = (
                    <tr key={uuidv4()}>
                        {showRowNumbers ? [rowNbrHdr, ...colHdrs] : colHdrs}
                    </tr>
                );

                const {
                    pageSize,
                    currPage,
                    currSort,
                    expandedRow,
                } = this.state;

                const {
                    column,
                    order,
                } = currSort;

                const sortedData = column ? sort(data, colSpec, column, order) : data;

                const currSlice
                    = showPagination
                    ? sortedData.slice((currPage - 1)* pageSize, (pageSize * currPage))
                    : sortedData;

                const expandRow = (idx) => {
                    if (typeof onRowClick === 'function') {
                        onRowClick(data[idx - 1]);
                    }
                    const { expandedRow } = this.state;
                    this.setState({ ...this.state, expandedRow: (expandedRow === idx) ? null : idx });
                };

                const rows = currSlice.map((rowData, idx) => {
                    const rowTd
                        = (showRowNumbers)
                        ? <td key={uuidv4()} className={styles.rowNumber}>{idx + 1 + ((currPage - 1) * pageSize)}</td>
                        : null;

                    const cols = colSpec.map((colInfo) => {
                        return <td key={uuidv4()} className={colInfo.styleClass || ''}>{colInfo.extract(rowData)}</td>;
                    });

                    return (
                        <tr
                            key={uuidv4()}
                            onClick={() => { (typeof rowExpansion === 'function') && expandRow(idx + 1); }}
                            style={(typeof rowExpansion === 'function') ? { cursor: 'pointer' } : { cursor: 'text' }}
                        >
                            {showRowNumbers ? [rowTd, ...cols] : cols}
                        </tr>
                    );
                });

                const treatedRows = () => {
                    if (expandedRow) {
                        const expRow = currSlice[expandedRow - 1];
                        const expRowComp = (
                            <tr key={uuidv4()} className={styles.expandedRow}>
                                <td colSpan={colSpec.length + (showRowNumbers ? 1 : 0)}>
                                    {rowExpansion(expRow)}
                                </td>
                            </tr>
                        );

                        return [
                            ...rows.slice(0, expandedRow),
                            expRowComp,
                            ...rows.slice(expandedRow),
                        ];
                    } else {
                        return rows;
                    }
                };

                return [
                    headers,
                    ...treatedRows(),
                ];
            };

            const {
                currPage,
                pageSize
            } = this.state;

            const prevSliceNav = (currPage > 1) ? (
                <div className={`${styles.pageNavItem} ${styles.prevPage}`}>
                    <a onClick={() => { this.prevPage(); }} className={simpleLink}>
                        {((currPage - 2) * pageSize) + 1} - {((currPage - 1) * pageSize)}
                    </a>
                </div>
            ) : null;

            const pageLast = (currPage + 1) * pageSize;
            const lastCount = (data.length < pageLast) ? data.length : pageLast;

            const currSliceNav = (
                <div className={`${styles.pageNavItem} ${styles.currPage}`}>
          <span className={`${styles.currPage}`}>
            {((currPage -1) * pageSize) + 1} - {lastCount}
          </span>
                </div>
            );

            const extra = (data.length % pageSize);
            const totalPages = ((data.length / pageSize) - 1) + ((extra > 0) ? 1 : 0);

            const nextNav = ((data.length > pageSize) && (currPage <= totalPages)) ? (
                <div className={`${styles.pageNavItem} ${styles.nextPage}`}>
                    <a onClick={() => { this.nextPage(); }} className={simpleLink}>
                        {(currPage *pageSize) + 1} - {lastCount}
                    </a>
                </div>
            ) : null;

            const tableHeader = showPagination ? (
                <div className={styles.tableHeader}>
                    <div className={styles.totalRecords}>Total <strong>{data.length}</strong> {totalRecordsText}</div>
                    <div style={{ display: 'flex' }}>
                        {paginationPrefix}
                        <div className={styles.pageNav}>
                            {prevSliceNav}
                            {currSliceNav}
                            {nextNav}
                        </div>
                    </div>
                </div>
            ) : null;

            return (
                <div style={{ width: '100%' }}>
                    {tableHeader}
                    <table className={`${styles.simpleTable} ${styleClass}`}>
                        <tbody>
                        {getRows()}
                        </tbody>
                    </table>
                </div>
            );
        }

        return (
            <table className={`${styles.simpleTable} ${styleClass}`}>
                <tbody>
                <tr key={uuidv4()}>
                    <td key={uuidv4()}>{noDataText}</td>
                </tr>
                </tbody>
            </table>
        );
    }
}

Table.defaultProps = {
    noDataText: 'No data',
    showRowNumbers: false,
    pageSize: 5,
    totalRecordsText: 'records',
    paginationPrefix: '',
    showPagination: true,
    styleClass: '',
};

Table.propTypes = {
    colSpec: PropTypes.arrayOf(TableColSpec).isRequired,
    data: PropTypes.array,
    noDataText: PropTypes.string,
    showRowNumbers: PropTypes.bool,
    showPagination: PropTypes.bool,
    pageSize: PropTypes.number,
    totalRecordsText: PropTypes.string,
    paginationPrefix: PropTypes.string,
    onRowClick: PropTypes.func,
    rowExpansion: PropTypes.func,
    styleClass: PropTypes.string,
    initSort: PropTypes.shape({
        id: PropTypes.string,
        order: PropTypes.oneOf([TABLE_SORT_ORDER_ASC, TABLE_SORT_ORDER_DESC])
    }),
};

export default Table;
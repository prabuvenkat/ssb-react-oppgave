import PropTypes from 'prop-types';

const shape = PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.any.isRequired,
    extract: PropTypes.func.isRequired,
    style: PropTypes.object,
    headerStyle: PropTypes.object,
    styleClass: PropTypes.string,
    headerStyleClass: PropTypes.string,
    sortable: PropTypes.bool,
    sortFn: PropTypes.func,
});

export default shape;
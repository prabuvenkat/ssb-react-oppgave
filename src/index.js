import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';

const mainComp = (
    <App />
);

ReactDOM.render(
    mainComp,
    document.getElementById('root')
);
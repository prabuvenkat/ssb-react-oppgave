import JST from 'jsonstat';
import { cloneDeep } from 'lodash';

export const simplifyDimensions = (jst) =>
    jst.Dimension().reduce((acc, d, dimId) => ([
        ...acc,
        {
            name: d.label,
            key: jst.id[dimId],
            idx: dimId,
            size: d.Category().length,
            category: d.Category().map((c, idx) => ({
                idx,
                id: d.id[idx],
                label: c.label,
            }))
        },
    ]), []);

class Dataset {
    constructor(dataset) {
        this._JST = JST(dataset);
        if (!this._JST) {
            throw new Error("Supplied dataset does not seem to be a valid JSON-Stat dataset.. aborting.");
        }

        this._dimensions = simplifyDimensions(this._JST);
    }

    get label() { return this._JST.label; }
    get length() { return this._JST.value ? this._JST.value.length : -1; }
    get dimensions() { return this._dimensions; }
    get roles() { return { time: this._JST.role.time[0], metric: this._JST.role.metric[0] }; }

    slice(filters = {}, { asTable = true, clone = false, ignoreHeaders = true } = {}) {
        const src = clone ? cloneDeep(this._JST) : this._JST;
        const slice = src.Slice(filters);
        return asTable ? slice.toTable().slice(ignoreHeaders ? 1 : 0) : slice;
    }

    treatCriteria(criteria) {
        if (! criteria) { return null; }

        const varKeys = Object.keys(criteria);
        console.log('criteria, varKeys', criteria, varKeys);
        if (varKeys.length > 0) {
            return varKeys.reduce((acc, k) => {
                console.log('Looking for', k, 'in', this._dimensions);
                const dim = this._dimensions.find(d => d.key === k);
                const inc = {
                    [dim.idx]: criteria[k],
                };
                console.log('inc', inc);
                return inc;
            }, {});
        } else {
            return null;
        }
    }

    narrow(criteria = {}, { ignoreHeaders = true} = {}) {
        const tblOptions = { type: 'arrayObj', content: 'id' };
        const treatedCriteria = this.treatCriteria(criteria);
        let acc = [];
        this._JST.toTable(tblOptions, (d, i) => {
            if (i !== 0) {
                if (treatedCriteria && (Object.keys(treatedCriteria).length > 0)) {
                    return d.forEach((f, idx) => {
                        if (Object.keys(treatedCriteria).includes(`${idx}`)) {
                            const crt = treatedCriteria[idx];
                            const crtArray = Array.isArray(crt) ? crt : [crt];

                            if (crtArray.includes(f)) {
                                acc.push(d);
                            }
                        }
                    });
                } else {
                    console.log('no criteria specified! including item as is');
                    acc.push(d);
                }
            }
        });
        return acc;
    }
}

export default Dataset;
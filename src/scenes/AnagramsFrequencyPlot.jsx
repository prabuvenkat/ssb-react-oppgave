import React from 'react';
import { makeFrequencyChart } from '../utils/chartUtils';

const AnagramsFrequencyPlot = ({ anagrams }) => {
    const frequencyMap = anagrams.reduce((acc, set) => ({
        ...acc,
        [set.length]: (acc[set.length] || 0) + 1,
    }), {});

    return makeFrequencyChart(frequencyMap, 'Antall anagrammer', 'Frekvens');
};

export default AnagramsFrequencyPlot;
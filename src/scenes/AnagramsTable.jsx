import React from 'react';
import Table from '../components/common/table/Table.jsx';

import styles from './anagrams.css';

const ANAGRAMS_COLSPEC = [{
        id: 'anagrams',
        title: 'Anagrammer',
        extract: words => Array.isArray(words) ? words.join(' ') : '',
    }, {
        id: 'count',
        title: 'Antall',
        extract: words => Array.isArray(words) ? words.length : '--',
        sortable: true,
        sortFn: (a1, a2) => a2.length - a1.length,
        styleClass: styles.anagramCount,
        headerStyleClass: styles.anagramCount,
    },
];

const AnagramsTable = ({anagrams, totalWordCount}) => {
    return (
        <Table
            colSpec={ANAGRAMS_COLSPEC}
            data={anagrams}
            pageSize={50}
            totalRecordsText={`anagrammer (av ${totalWordCount} innføringer)`}
            showRowNumbers={true}
        />
    );
};

export default AnagramsTable;
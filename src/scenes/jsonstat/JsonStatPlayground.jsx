import React, { useState, useEffect } from 'react';
import JsonStatTable from './JsonStatTable.jsx';
import FactsBeforeFeelings from './social/FactsBeforeFeelings.jsx';
import DropDown from '../../components/common/dropdown/Dropdown.jsx';
import Modal from '../../components/common/dialog/Modal.jsx';
import {
    makeVariableOptions,
    makeDimensionOptions,
    toDatasetSliceFilters,
} from '../../utils/jsonstatUtils.js';
import { makeJsonStatPlot } from '../../utils/jsonstatUtils';
import { VIEW_MODE } from '../../utils/helpers';
import { exportChartAsImage } from '../../service/http';

import appStyles from '../../main.css';

const selected = (val) => val && (val !== '--');

const JsonStatPlayground = ({ dataset }) => {
    const { time, metric } = dataset.roles;
    const [xAxisVariable, setxAxisVariable] = useState(dataset.dimensions.find(d => d.key === time).name || '--');
    const [yAxisVariable, setyAxisVariable] = useState(dataset.dimensions.find(d => d.key === metric).name || '--');
    const [sliceVariables, setSliceVariables] = useState({});

    const [xAxisOptions, setxAxisOptions] = useState();
    const [yAxisOptions, setyAxisOptions] = useState();
    const [sliceDropdowns, setSliceDropdowns] = useState([]);

    const [dataSlice, setDataSlice] = useState();
    const [viewMode, setViewMode] = useState(VIEW_MODE.VIZ);
    const [renderedChart, setRenderedChart] = useState();
    const [expImgSrc, setExpImgSrc] = useState();
    const [showSocialShare, setShowSocialShare] = useState(false);

    useEffect(() => {
        const options = makeVariableOptions(dataset.dimensions,
            { includeLeaderOption: true, leaderText: `Velg x-akse` });

        setxAxisOptions(options);
    }, []);

    useEffect(() => {
        if (selected(xAxisVariable)) {
            setyAxisOptions(
                makeVariableOptions(dataset.dimensions,
                { exclude: [ xAxisVariable ], includeLeaderOption: true , leaderText: `Velg y-akse`})
            );
            // setyAxisVariable('--');
        }
    }, [xAxisVariable]);

    useEffect(() => {
        if (selected(xAxisVariable) && selected(yAxisVariable)) {
            setSliceDropdowns(dataset.dimensions
                .filter(d => ![xAxisVariable, yAxisVariable].includes(d.name))
                .map(d => {
                    const options = makeDimensionOptions(d, {
                        includeLeaderOption: true,
                        leaderText: `Velg 1 ${d.name} verdi`,
                    });
                    const dimDropDown = (
                        <DropDown
                            id={`${d.name}-selector`}
                            options={options}
                            selection={sliceVariables[d.name] || '--'}
                            onChange={({ target: { value }}) => {
                                setSliceVariables({
                                    ...sliceVariables,
                                    [d.name]: value,
                                });
                            }}
                            prefix={d.key}
                        />
                    );
                    return dimDropDown;
                })
            );
        }
    }, [xAxisVariable, yAxisVariable, sliceVariables]);

    useEffect(() => {
        if (sliceVariables && (Object.keys(sliceVariables).length > 0)) {
            const filters = toDatasetSliceFilters(dataset, sliceVariables);
            setDataSlice(dataset.slice(filters, { clone: true }));
        }
    }, [sliceVariables]);

    const onPlotRendered = async (chart) => {
        setRenderedChart(chart);
    };

    const toggleSocialShare = () => {
        setShowSocialShare(! showSocialShare);
    };

    const doShare = () => {
        toggleSocialShare();
    };

    const exportForSocialShare = async () => {
        const base64ImageSrc = await exportChartAsImage(renderedChart.userOptions);
        setExpImgSrc(base64ImageSrc);
    };

    const xAxisSelector = xAxisOptions ? (
        <DropDown
            id="xAxisSelector"
            options={xAxisOptions}
            selection={xAxisVariable}
            onChange={({ target: { value }}) => {
                setxAxisVariable(value);
            }}
            disabled={!! dataSlice}
            prefix="x"
        />
    ) : null;

    const yAxisSelector = yAxisOptions ? (
        <DropDown
            id="yAxisSelector"
            options={yAxisOptions}
            selection={yAxisVariable}
            onChange={({ target: { value }}) => { setyAxisVariable(value); }}
            disabled={!! dataSlice}
            prefix="y"
        />
    ) : null;

    const dataTable = dataSlice ? (
        <JsonStatTable dimensions={dataset.dimensions} data={dataSlice} />
    ) : null;

    const dataViz = dataSlice
        ? makeJsonStatPlot(dataset.dimensions, dataSlice, xAxisVariable, yAxisVariable, onPlotRendered)
        : null;

    return (
        <div>
            <div className={appStyles.actionBar}>
                <div className={appStyles.variableSelection}>
                    {xAxisSelector}
                    {yAxisSelector}
                    &middot; {sliceDropdowns}
                </div>
                <div className={appStyles.actionIcons}>
                    <a
                        className={`${appStyles.actionIcon} ${viewMode === VIEW_MODE.VIZ ? appStyles.selected : ''}`}
                        onClick={() => { setViewMode(VIEW_MODE.VIZ); }}
                    >
                        <i className="material-icons">insert_chart</i>
                    </a>
                    <a
                        className={`${appStyles.actionIcon} ${viewMode === VIEW_MODE.TABLE ? appStyles.selected : ''}`}
                        onClick={() => { setViewMode(VIEW_MODE.TABLE); }}
                    >
                        <i className="material-icons">view_list</i>
                    </a>
                    {renderedChart &&
                        <a
                            className={`${appStyles.actionIcon}`}
                            onClick={() => {
                                exportForSocialShare();
                                toggleSocialShare();
                            }}
                        >
                            <i className="material-icons">share</i>
                        </a>
                    }
                </div>
            </div>
            <div className={appStyles.mainScene}>
                {(viewMode === VIEW_MODE.VIZ) && dataViz}
                {(viewMode === VIEW_MODE.TABLE) && dataTable}
            </div>
            <Modal
                onConfirm={doShare} confirmText="Share !"
                onCancel={toggleSocialShare} showCancel={false}
                show={showSocialShare}
                styleClass={appStyles.socialShare}
            >
                {expImgSrc &&
                    <FactsBeforeFeelings
                        factInfo={dataset}
                        factChart={<img src={expImgSrc} width="550px" />}
                    />
                }
            </Modal>
        </div>
    );
};

export default JsonStatPlayground;
import React from 'react';
import Table from '../../components/common/table/Table.jsx';
import { dimensionsAsColSpec } from '../../utils/jsonstatUtils';

const JsonStatTable = ({ dimensions, data }) => {
    return (
        <Table
            colSpec={dimensionsAsColSpec(dimensions)}
            data={data}
            pageSize={data.length}
        />
    );
};

export default JsonStatTable;
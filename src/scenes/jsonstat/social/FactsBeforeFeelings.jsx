import React, { useState, useEffect } from 'react';

import styles from './fbf.css';
import img from '../../../data/ssb-logo.png';

const FactsBeforeFeelings = ({ factChart, factInfo }) => {
    const [msg, setMsg] = useState('Here is a fact for thought --- ');

    return (
        <div className={styles.factsInfoDialog}>
            <p>{msg}</p>
            {factChart}
            <div className={styles.factsInfoPanel}>
                <div className={styles.factsInfoLogo}>
                    <img src={img} />
                </div>
                <div className={styles.factsInfoContent}>
                    <p><b>Dataset</b>: {factInfo.label} [<a href="#">source</a>]</p>
                </div>
            </div>
        </div>
    );
};

export default FactsBeforeFeelings;
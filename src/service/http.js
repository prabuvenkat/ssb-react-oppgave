const fetch = require('node-fetch');

const EXPORT_SERVER_URL = 'http://localhost:7801';
const IMG_PREAMBLE = 'data:image/png;base64,';

export const exportChartAsImage = async (payload) => {
    console.log('trying to export', payload);
    const url = EXPORT_SERVER_URL;
    const options = {
        method: 'post',
        body: JSON.stringify({ type: 'png', infile: payload }),
        headers: { 'Content-Type': 'application/json' },
    };

    const response = await fetch(url, options);
    const imgData = new Uint8Array(await response.arrayBuffer())
        .reduce((acc, b) => acc + String.fromCharCode(b), '');

    return IMG_PREAMBLE + btoa(imgData);
};
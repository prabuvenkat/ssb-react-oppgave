import React from 'react';
import HighchartsCore from 'highcharts';
import Highcharts from 'highcharts-react-official';

export const LINE_CHART = 'line';
export const HISTOGRAM = 'column';

export const makeFrequencyChart = (frequencies, xAxisLabel, yAxisLabel) => {
    const categories = Object.keys(frequencies);
    const xAxis = [{ categories }];
    const yAxis = { title: { text: yAxisLabel }};
    const data = categories.map(cat => [cat, frequencies[cat]]);

    const options = {
        chart: {
            type: HISTOGRAM,
        },

        title: 'Frekvens',
        xAxis,
        yAxis,
        series: [{
            name: xAxisLabel,
            data,
        }],

        tooltip: {
            useHTML: true,
            headerFormat: 'Ord med <span style="font-weight: bold;">{point.key}</span> anagrammer:<br />',
            pointFormat: '<span style="font-weight: bold;">Frekvens</span>: {point.y}',
        },
    };

    return (
        <Highcharts
            highcharts={HighchartsCore}
            options={options}
        />
    );
};

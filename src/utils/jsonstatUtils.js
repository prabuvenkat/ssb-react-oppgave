import React from 'react';
import { chunk } from 'lodash';
import HighchartsCore from 'highcharts';
import Highcharts from 'highcharts-react-official';
import { LINE_CHART } from './chartUtils';

const VARIABLE_DROPDOWN_OPTIONS = {
    exclude: [],
    includeLeaderOption: true,
    leaderText: '-- Select --',
    disabled: [],
};

export const makeVariableOptions = (dimensions, options = VARIABLE_DROPDOWN_OPTIONS) => {
    const base = options.includeLeaderOption ? { '--' : `${options.leaderText}` } : {};
    return dimensions.reduce((acc, d) => {
        if ((options.exclude || []).includes(d.name)) { return acc; }
        return {
            ...acc,
            [d.name]: d.key,
        };
    }, base);
};

const DIMENSION_DROPDOWN_OPTIONS = {
    exclude: [],
    includeLeaderOption: true,
    leaderText: '-- Select --',
    disabled: [],
};

export const makeDimensionOptions = (dimension, options = DIMENSION_DROPDOWN_OPTIONS) => {
    const base = options.includeLeaderOption ? { '--' : `Select ${dimension.key}` } : {};
    return dimension.category.reduce((acc, c) => {
        if ((options.exclude || []).includes[c.id]) { return acc; }
        return {
            ...acc,
            [c.id]: c.label,
        };
    }, base);
};

export const toDatasetSliceFilters = (dataset, sliceVariables) => {
    const sliceKeys = Object.keys(sliceVariables);
    if (sliceKeys.length < 1) {
        return {};
    }

    return sliceKeys.reduce((acc, k) => {
        const d = dataset.dimensions.find(d => d.name === k);
        return {
            ...acc,
            [d.key]: sliceVariables[k],
        };
    }, {});
};

export const dimensionsAsColSpec = (dimensions) => {
    return [
        ...dimensions.map((d, idx) => ({
            title: d.key,
            id: d.name,
            extract: v => v[idx],
        })),
        {
            title: 'Value',
            id: 'value',
            extract: v => v[dimensions.length],
        },
    ];
};

export const makeJsonStatPlot = (dimensions, data, xAxisDimId, yAxisDimId, callback) => {
    const xDim = dimensions.find(d => d.name === xAxisDimId);
    const yDim = dimensions.find(d => d.name === yAxisDimId);

    const xAxis = [{ categories: xDim.category.map(c => c.label)}];
    const yAxis = [{ title: { text: yDim.key }}];

    const series = chunk(data, xDim.size).map((chunk, idx) => {
        return {
            name: yDim.category[idx % yDim.size].label,
            data: chunk.map(cx => cx[dimensions.length] || 0),
        };
    });

    const options = {
        chart: { type: LINE_CHART },
        title: 'JsonStat chart',
        xAxis,
        yAxis,
        series,
        tooltip: {
            useHTML: true,
            headerFormat: `${xDim.key} : <b>{point.key}</b><br />`,
            pointerFormat: `${yDim.key} : <b>{point.y}</b><br />`,
        },
    };

    console.log('chart options', data, options);
    return (
        <Highcharts
            highcharts={HighchartsCore}
            options={options}
            callback={callback}
        />
    );
};

export const isEmpty = (str) => {
    return !(str && (str.length > 0));
};

export const sortChars = (word, options = { reverse: false }) => {
    const sortedArray = [...word].sort();
    return (options.reverse ? sortedArray.reverse() : sortedArray).join('');
};

export const computeAnagrams = (wordList = []) => {
    const bucket = wordList.reduce((acc, word) => {
        if (! isEmpty(word)) {
            const sorted = sortChars(word.toLowerCase());
            return {
                ...acc,
                [sorted]: [ ...(acc[sorted] || []), word ],
            };
        }

        return acc;
    }, {});

    return Object.keys(bucket)
        .filter(k => bucket[k].length > 1)
        .map(k => bucket[k]);
};
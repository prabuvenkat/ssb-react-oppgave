const webpack = require('webpack');
const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const dotenv = require('dotenv');

const env = dotenv.config({ path: __dirname + '/.env.development.local' }).parsed;
const ENV = env ? Object.keys(env).reduce((acc, key) => {
    acc[`process.env.${key}`] = JSON.stringify(env[key]);
    return acc;
}, {}) : {};

const config = {
    entry: {
        scripts: './src/index.js'
    },

    output: {
        path: path.join(__dirname, 'build'),
        filename: 'bundle.js',
    },

    plugins: [
        new webpack.DefinePlugin(ENV),
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: 'index.html',
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        })
    ],

    mode: 'development',

    module: {
        rules: [
            {
                test: /\.txt$/,
                use: 'raw-loader'
            },
            {
                test: /\.(png|jpg|svg)$/,
                use: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[name]__[local]__[hash:base64:5]'
                            }
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            config: {
                                path: path.resolve(__dirname, 'postcss.config.js')
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            }
        ]
    },

    node: {
        fs: 'empty'
    },

    stats: {
        colors: true
    },

    devServer: {
        contentBase: './build',
        disableHostCheck: true,
    },

    devtool: 'source-map'
};

module.exports = config;